//
//  KineticAnimationObject.swift
//  MadJohTools
//
//  Created by Valentin Barat on 06/06/2017.
//  Copyright © 2017 MadJoh. All rights reserved.
//

import Foundation

public class KineticAnimationObject: NSObject {
    public var value: Double = 0
    public init(_ value: Double = 0) { super.init(); self.value = value }
    public convenience init(valueInt: Int = 0) { self.init(Double(valueInt)) }
}
