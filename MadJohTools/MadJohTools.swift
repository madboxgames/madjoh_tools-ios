//
//  MadJohTools.swift
//  Versus
//
//  Created by Valentin Barat on 29/12/2016.
//  Copyright © 2016 madjoh. All rights reserved.
//

import Foundation
import UIKit

public class MadJohTools
{
    public static var textFont: UIFont!
    
    // DELAY METHODS
    public static func delay(_ delay: Double, closure:@escaping ()->())
    {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    
    public static func repeatCode(_ delay: Double, closure:@escaping () -> Bool)
    {
        if (closure())
        {
            MadJohTools.delay(delay) {
                MadJohTools.repeatCode(delay, closure: closure)
            }
        }
    }
    
    public static func delay(preciseDelay: Double, closure: @escaping (Double)->())
    {
        var currTime = CACurrentMediaTime()
        var loop : GameLoop! = nil
        var totalTime : Double = 0
        
        let update = {
            let cTime = CACurrentMediaTime()
            let time = cTime - currTime
            
            totalTime += time
            
            if (totalTime >= preciseDelay)
            {
                closure(totalTime - preciseDelay)
                loop.stop()
                return
            }
            
            currTime = cTime
        }
        
        loop = GameLoop(frameInterval: 1, doSomething: update)
        loop.start()
    }
    
    public static func repeatCode(preciseDelay: Double, extraTime: Double = 0, closure:@escaping () -> Bool)
    {
        let date = Date()
        if (closure())
        {
            delay(preciseDelay: preciseDelay - extraTime + date.timeIntervalSinceNow) { extraTime in
                repeatCode(preciseDelay: preciseDelay, extraTime: extraTime, closure: closure)
            }
        }
    }
    
    // ENUMS
    public static func iterateEnum<T: Hashable>(_: T.Type) -> AnyIterator<T> {
        var i = 0
        return AnyIterator {
            let next = withUnsafeBytes(of: &i) { $0.load(as: T.self) }
            if next.hashValue != i { return nil }
            i += 1
            return next
        }
    }
    
    // BUTTONS
    public static func getButtonWithIcon(_ txt: String, image: UIImage, width: CGFloat, height: CGFloat, color: UIColor, txtColor: UIColor = UIColor.white, maxTextSize: CGFloat = -1, txtAlignement: NSTextAlignment = .left) -> UIButton
    {
        let offset = 8 as CGFloat
        let cell = UIButton(frame: CGRect(x: 0, y: 0, width: width, height: height))
        cell.layer.masksToBounds = true
        cell.backgroundColor = color
        
        let img = UIImageView(image: image)
        img.tag = 1
        cell.addSubview(img)
        
        let lblWidth = width - 2 * offset - img.width
        let lbl = UILabel()
        lbl.text = txt
        lbl.textColor = txtColor
        lbl.textAlignment = txtAlignement
        lbl.numberOfLines = 1
        lbl.lineBreakMode = .byTruncatingTail
        lbl.adjustFont(maxTextSize: ((maxTextSize == -1) ? height : maxTextSize), maxWidth: lblWidth)
        cell.addSubview(lbl)
        
        img.frame = CGRect(x: offset, y: (height - img.height) / 2, width: img.width, height: img.height)
        lbl.frame = CGRect(x: 2 * offset + img.width, y: (height - lbl.height) / 2, width: lblWidth, height: lbl.height)
        
        return cell
    }
    
    public static let MadJohColor = UIColor(hexString: "#B1CED6FF")
    public static func getMadJohButton(_ txt: String, width: CGFloat, height: CGFloat, maxTextSize: CGFloat = -1) -> UIButton
    {
        return getButtonWithIcon(txt, image: UIImage.getScaledWithHeight("madjoh_no_text", height: height * 0.6), width: width, height: height, color: MadJohColor!, txtColor: UIColor.white, maxTextSize: maxTextSize)
    }
}
