//
//  CircleView.swift
//  MadJohTools
//
//  Created by Valentin Barat on 06/06/2017.
//  Copyright © 2017 MadJoh. All rights reserved.
//

import UIKit
import Foundation

public class CircleView: UIView {
    
    var circleLayer: CAShapeLayer!
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
        
        // Use UIBezierPath as an easy way to create the CGPath for the layer.
        // The path should be the entire circle.
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: frame.size.width / 2.0, y: frame.size.height / 2.0), radius: frame.width/4, startAngle: -CGFloat(Double.pi / 2.0), endAngle: CGFloat(3.0 * Double.pi / 2.0), clockwise: true)
        
        // Setup the CAShapeLayer with the path, colors, and line width
        circleLayer = CAShapeLayer()
        circleLayer.path = circlePath.cgPath
        circleLayer.fillColor = UIColor.clear.cgColor
        
        //I'm going to change this in the ViewController that uses this. Not the best way, I know but alas.
        circleLayer.strokeColor = UIColor.clear.cgColor
        
        //You probably want to change this width
        circleLayer.lineWidth = frame.width/2;
        
        // Don't draw the circle initially
        circleLayer.strokeEnd = 0.0
        
        // Add the circleLayer to the view's layer's sublayers
        layer.addSublayer(circleLayer)
        
    }
    
    public func setStrokeColor(color : CGColor) {
        circleLayer.strokeColor = color
    }
    
    // This is what you call if you want to draw a full circle.
    public func animateCircle(duration: Double) {
        animateCircleTo(duration: duration, fromValue: 0.0, toValue: 1.0)
    }
    
    // This is what you call to draw a partial circle.
    public func animateCircleTo(duration: Double, fromValue: CGFloat, toValue: CGFloat){
        // We want to animate the strokeEnd property of the circleLayer
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.isRemovedOnCompletion = false
        
        // Set the animation duration appropriately
        animation.duration = duration
        
        // Animate from 0 (no circle) to 1 (full circle)
        animation.fromValue = 0
        animation.toValue = toValue
        
        // Do an easeout. Don't know how to do a spring instead
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        
        // Set the circleLayer's strokeEnd property to 1.0 now so that it's the
        // right value when the animation ends.
        circleLayer.strokeEnd = toValue
        
        // Do the actual animation
        circleLayer.add(animation, forKey: "animateCircle")
    }
    
    public func pauseAnimation() {
        let pausedTime: CFTimeInterval = circleLayer.convertTime(CACurrentMediaTime(), from: nil)
        circleLayer.speed = 0.0
        circleLayer.timeOffset = pausedTime
    }
    
    public func resumeLayer() {
        let pausedTime: CFTimeInterval = circleLayer.timeOffset
        circleLayer.speed = 1.0
        circleLayer.timeOffset = 0.0
        circleLayer.beginTime = 0.0
        let timeSincePause: CFTimeInterval = circleLayer.convertTime(CACurrentMediaTime(), from: nil) - pausedTime
        circleLayer.beginTime = timeSincePause
    }
    
    
    // required function
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
