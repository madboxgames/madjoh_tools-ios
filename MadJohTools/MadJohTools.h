//
//  MadJohTools.h
//  MadJohTools
//
//  Created by Valentin Barat on 29/12/2016.
//  Copyright © 2016 MadJoh. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for MadJohTools.
FOUNDATION_EXPORT double MadJohToolsVersionNumber;

//! Project version string for MadJohTools.
FOUNDATION_EXPORT const unsigned char MadJohToolsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MadJohTools/PublicHeader.h>


