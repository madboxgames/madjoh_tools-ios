//
//  classExtensions.swift
//  Versus
//
//  Created by Valentin Barat on 23/01/2016.
//  Copyright © 2016 madjoh. All rights reserved.
//

import UIKit
import SpriteKit


// Adding a overload constructor to be able to use RGBA format
// Param hexString: "#RRGGBBAA" in hexadecimal
// Return: corresponding UIColor
public extension UIColor
{
    public convenience init?(hexString: String)
    {
        let r, g, b, a: CGFloat
        
        if hexString.hasPrefix("#")
        {
            let start = hexString.characters.index(hexString.startIndex, offsetBy: 1)
            let hexColor = hexString.substring(from: start)
            
            if hexColor.characters.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0
                
                if scanner.scanHexInt64(&hexNumber)
                {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255
                    
                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }
        return nil
    }
    
    public convenience init?(argb: String)
    {
        var argbLocal = "\(argb)"
        if(!argb.hasPrefix("#")) { argbLocal = "#\(argb)" }
        let startRGB = argbLocal.characters.index(argbLocal.startIndex, offsetBy: 3)
        let startA = argbLocal.characters.index(argbLocal.startIndex, offsetBy: 1)
        let rgb = argbLocal.substring(from: startRGB)
        let a = argbLocal.substring(with: Range(uncheckedBounds: (startA, startRGB)))
        
        self.init(hexString: "#\(rgb)\(a)")
        return
        
    }
}

public extension String {
    public static func random(_ length: Int) -> String {
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
    
    public func isEmail() -> Bool {
        let regExp = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTester = NSPredicate(format:"SELF MATCHES %@", regExp)
        return emailTester.evaluate(with: self)
    }
    
    public var localized:String {
        return NSLocalizedString(self, comment: "")
    }
    
    public func localizedWithDefault(_ value: String) -> String
    {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: value, comment: "")
    }
}

public extension Double {
    /// Rounds the double to decimal places value
    public func roundToPlaces(_ places : Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
    public func format(_ f: Int) -> String {
        return String(format: "%.\(f)f", self)
    }
    
    /// Returns a random floating point number between 0.0 and 1.0, inclusive.
    public static var random: Double
    {
        get
        {
            return Double(arc4random()) / 0xFFFFFFFF
        }
    }
    /**
    Create a random number Double
    
    - parameter min: Double
    - parameter max: Double
         
    - returns: Double
    */
    public static func random(min: Double, max: Double) -> Double {
        return Double.random * (max - min) + min
    }
    
    public var degreesToRadians : CGFloat
    {
        return CGFloat(self) * .pi / 180.0
    }
}

public extension Int
{
    public var degreesToRadians : CGFloat
    {
        return CGFloat(self) * .pi / 180.0
    }
    
    public static var random : Int
    {
        get
        {
            return Int(arc4random_uniform(99999999))
        }
    }
}

public extension CGPoint
{
    public func normalize() -> CGPoint
    {
        let length = sqrt(self.x * self.x + self.y * self.y)
        return CGPoint(x: self.x / length, y: self.y / length)
    }
}

// Extensions to all UIView's children
public extension UIView
{
    public func createLoader(ofSize: CGFloat = 50, style: UIActivityIndicatorViewStyle = .white, center: CGPoint? = nil) -> UIActivityIndicatorView
    {
        let x = center == nil ? (self.width - ofSize) / 2 : center!.x - ofSize / 2
        let y = center == nil ? (self.height - ofSize) / 2 : center!.y - ofSize / 2
        let activity = UIActivityIndicatorView(frame: CGRect(x: x, y: y, width: ofSize, height: ofSize))
        activity.activityIndicatorViewStyle = style
        activity.hidesWhenStopped = true
        activity.startAnimating()
        
        self.addSubview(activity)
        return activity
    }
    
    public func moveTo(_ f: CGRect, duration: Double, delay: Double = 0, options: UIViewAnimationOptions = [.curveEaseInOut, .allowUserInteraction], completion: ((Bool)->())? = nil)
    {
        UIView.animate(withDuration: duration, delay: delay, options: options, animations: {
            self.frame = f
        }, completion: completion)
    }
    
    public func moveTo(point: CGPoint, duration: Double, delay: Double = 0, options: UIViewAnimationOptions = [.curveEaseInOut, .allowUserInteraction], completion: ((Bool)->())? = nil)
    {
        UIView.animate(withDuration: duration, delay: delay, options: options, animations: {
            self.frame.origin = point
        }, completion: completion)
    }
    
    public func moveBy(x: CGFloat = 0, y: CGFloat = 0, duration: Double, delay: Double = 0, options: UIViewAnimationOptions = [.curveEaseInOut, .allowUserInteraction], completion: ((Bool)->())? = nil)
    {
        if (x == 0 && y == 0) { completion?(true); return }
        
        UIView.animate(withDuration: duration, delay: delay, options: options, animations: {
            if (x != 0) { self.x += x }
            if (y != 0) { self.y += y }
        }, completion: completion)
    }
    
    public func removeAllSubviews()
    {
        for v in subviews
        {
            v.removeFromSuperview()
        }
    }
    
    public func disableAllSubviewsUsersInteraction()
    {
        for v in self.subviews
        {
            v.isUserInteractionEnabled = false
        }
    }
    
    public func rotateBy(radiant: CGFloat)
    {
        self.transform = self.transform.rotated(by: -radiant)
    }
    
    public func rotateBy(degrees: Int)
    {
        rotateBy(radiant: degrees.degreesToRadians)
    }
    
    // Round the given corner of the view
    // Usage: view.roundCorners([.TopLeft, .TopRight], radius: 20)
    public func roundCorners(_ corners:UIRectCorner, radius: CGFloat)
    {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    /**
     Get Set x Position
     
     - parameter x: CGFloat
     by DaRk-_-D0G
     */
    public var x:CGFloat {
        get {
            return self.frame.origin.x
        }
        set {
            self.frame.origin.x = newValue
        }
    }
    /**
     Get Set y Position
     
     - parameter y: CGFloat
     by DaRk-_-D0G
     */
    public var y:CGFloat {
        get {
            return self.frame.origin.y
        }
        set {
            self.frame.origin.y = newValue
        }
    }
    /**
     Get Set Height
     
     - parameter height: CGFloat
     by DaRk-_-D0G
     */
    public var height:CGFloat {
        get {
            return self.frame.size.height
        }
        set {
            self.frame.size.height = newValue
        }
    }
    /**
     Get Set Width
     
     - parameter width: CGFloat
     by DaRk-_-D0G
     */
    public var width:CGFloat {
        get {
            return self.frame.size.width
        }
        set {
            self.frame.size.width = newValue
        }
    }
    
    public func clone() -> AnyObject
    {
        return NSKeyedUnarchiver.unarchiveObject(with: NSKeyedArchiver.archivedData(withRootObject: self))! as AnyObject
    }
}

public extension UILabel
{
    public func addLogoToText(logo: UIImage)
    {
        let attachment = NSTextAttachment()
        attachment.image = logo
        let attachmentString = NSAttributedString(attachment: attachment)
        let myString = NSMutableAttributedString(string: self.text!)
        self.text = nil
        myString.append(attachmentString)
        self.attributedText = myString
    }
    
    public func adjustFont(_ maxFont: UIFont, maxWidth: CGFloat, fontName: String? = nil)
    {
        adjustFont(maxTextSize: maxFont.pointSize, maxWidth: maxWidth, fontName: fontName ?? maxFont.fontName)
    }
    
    public func adjustFont(_ maxFont: UIFont, maxHeight: CGFloat, fontName: String? = nil)
    {
        adjustFont(maxTextSize: maxFont.pointSize, maxHeight: maxHeight, fontName: fontName ?? maxFont.fontName)
    }
    
    public func adjustFont(_ maxFont: UIFont, maxWidth: CGFloat, maxHeight: CGFloat, fontName: String? = nil)
    {
        adjustFont(maxTextSize: maxFont.pointSize, maxWidth: maxWidth, maxHeight: maxHeight, fontName: fontName ?? maxFont.fontName)
    }
    
    public func adjustFont(maxTextSize: CGFloat, maxWidth: CGFloat, fontName: String? = nil)
    {
        var size = CGFloat(maxTextSize)
        var font = fontName == nil ? MadJohTools.textFont == nil ? UIFont.systemFont(ofSize: size) :UIFont(name: MadJohTools.textFont.fontName, size: size) : (UIFont(name: fontName!, size: size))
        
        self.font = font
        self.sizeToFit()
        
        while (self.width > maxWidth && size > 1) {
            size -= 1
            font = fontName == nil ? MadJohTools.textFont == nil ? UIFont.systemFont(ofSize: size) :UIFont(name: MadJohTools.textFont.fontName, size: size) : (UIFont(name: fontName!, size: size))
            self.font = font
            self.sizeToFit()
        }
    }
    
    public func adjustFont(maxTextSize: CGFloat, maxHeight: CGFloat, fontName: String? = nil)
    {
        var size = CGFloat(maxTextSize)
        var font = fontName == nil ? MadJohTools.textFont == nil ? UIFont.systemFont(ofSize: size) :UIFont(name: MadJohTools.textFont.fontName, size: size) : (UIFont(name: fontName!, size: size))
        
        self.font = font
        self.sizeToFit()
        
        while (self.height > maxHeight && size > 1) {
            size -= 1
            font = fontName == nil ? MadJohTools.textFont == nil ? UIFont.systemFont(ofSize: size) :UIFont(name: MadJohTools.textFont.fontName, size: size) : (UIFont(name: fontName!, size: size))
            self.font = font
            self.sizeToFit()
        }
    }
    
    public func adjustFont(maxTextSize: CGFloat, maxWidth: CGFloat, maxHeight: CGFloat, fontName: String? = nil)
    {
        self.adjustFont(maxTextSize: maxTextSize, maxWidth: maxWidth, fontName: fontName)
        let size = self.font.pointSize
        self.adjustFont(maxTextSize: maxTextSize, maxWidth: maxHeight, fontName: fontName)
        let size2 = self.font.pointSize
        
        self.font = fontName == nil ? UIFont(name: MadJohTools.textFont.fontName, size: min(size, size2)) : (UIFont(name: fontName!, size: min(size, size2)) ?? UIFont.systemFont(ofSize: min(size, size2)))
    }
}

public extension UIImage {
    
    public var width:CGFloat {
        get {
            return self.size.width
        }
    }
    
    public var height:CGFloat {
        get {
            return self.size.height
        }
    }
    
    public func getGrayScale() -> UIImage
    {  
        let context = CIContext(options: nil)
        let currentFilter = CIFilter(name: "CIPhotoEffectNoir")
        currentFilter!.setValue(CIImage(image: self), forKey: kCIInputImageKey)
        let output = currentFilter!.outputImage
        let cgimg = context.createCGImage(output!,from: output!.extent)
        var processedImage = UIImage(cgImage: cgimg!)
        processedImage = processedImage.getScaledWithWidth(width: self.width)
        return processedImage
    }
    
    // returns a copy of 'image' thats fit in 'size'
    public func scaleToSize(size: CGSize) -> UIImage
    {
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Automatically use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(size, !hasAlpha, scale)
        self.draw(in: CGRect(origin: CGPoint.zero, size: size))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return scaledImage == nil ? self : scaledImage!
    }
    
    public static func getScaledWithWidth(_ image: String, width: CGFloat) -> UIImage
    {
        let img = UIImage(named: image)
        return img!.getScaledWithWidth(width: width)
    }
    
    public func getScaledWithWidth(width: CGFloat) -> UIImage
    {
        let ratio = width / self.size.width
        let height = (self.size.height) * ratio
        
        return self.scaleToSize(size: CGSize(width: width, height: height))
    }
    
    public static func getScaledWithHeight(_ image: String, height: CGFloat) -> UIImage
    {
        let img = UIImage(named: image)
        return img!.getScaledWithHeight(height: height)
    }
    
    public func getScaledWithHeight(height: CGFloat) -> UIImage
    {
        let ratio = height / self.size.height
        let width = self.size.width * ratio
        
        return self.scaleToSize(size: CGSize(width: width, height: height))
    }
    
    public func getScaledThatFitsIn(maxWidth: CGFloat, maxHeight: CGFloat) -> UIImage
    {
        let imgWidth = self.getScaledWithWidth(width: maxWidth)
        if (imgWidth.height <= maxHeight) { return imgWidth }
        return self.getScaledWithHeight(height: maxHeight)
    }
    
    public static func getScaledThatFitsIn(name: String, maxWidth: CGFloat, maxHeight: CGFloat) -> UIImage
    {
        let imgWidth = UIImage.getScaledWithWidth(name, width: maxWidth)
        if (imgWidth.height <= maxHeight) { return imgWidth }
        return UIImage.getScaledWithHeight(name, height: maxHeight)
    }
    
    public func getCroppedImageWithSize(size: CGFloat) -> UIImage
    {
        var imgScaled = self
        if (imgScaled.size.height > imgScaled.size.width)
        {
            imgScaled = imgScaled.getScaledWithWidth(width: size)
        }
        else
        {
            imgScaled = imgScaled.getScaledWithHeight(height: size)
        }
        imgScaled = imgScaled.cropImgToCenter(size: size)
        return imgScaled
    }
    
    public func cropImgToCenter(size: CGFloat) -> UIImage
    {
        let width = size
        let height = size
        
        let contextImage: UIImage = UIImage(cgImage: self.cgImage!)
        
        let contextSize: CGSize = contextImage.size
        
        var posX: CGFloat = 0.0
        var posY: CGFloat = 0.0
        var cgwidth: CGFloat = CGFloat(width)
        var cgheight: CGFloat = CGFloat(height)
        
        // See what size is longer and create the center off of that
        if contextSize.width > contextSize.height {
            posX = ((contextSize.width - contextSize.height) / 2)
            posY = 0
            cgwidth = contextSize.height
            cgheight = contextSize.height
        } else {
            posX = 0
            posY = ((contextSize.height - contextSize.width) / 2)
            cgwidth = contextSize.width
            cgheight = contextSize.width
        }
        
        let rect: CGRect = CGRect(x: posX, y: posY, width: cgwidth, height: cgheight)
        
        return cropImage(toFrame: rect)
    }
    
    public func cropImage(toFrame: CGRect) -> UIImage
    {
        let contextImage: UIImage = UIImage(cgImage: self.cgImage!)
        
        // Create bitmap image from context using the rect
        let imageRef: CGImage = contextImage.cgImage!.cropping(to: toFrame)!
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        let image: UIImage = UIImage(cgImage: imageRef, scale: self.scale, orientation: self.imageOrientation)
        
        return image
    }
}

public extension CGRect {
    public func getRectFromRatio(_ ratio: CGFloat) -> CGRect
    {
        return CGRect(x: self.origin.x * ratio, y: self.origin.y * ratio, width: self.width * ratio, height: self.height * ratio)
    }
}

public extension UIButton {
    private static let boucneDuration = 0.1
    private static let bounceScale : CGFloat = 0.9
    
    public func registerForBounce()
    {
        self.adjustsImageWhenHighlighted = false
        self.addTarget(UIButton.self, action: #selector(UIButton.grow(_:)), for: .touchDragExit)
        self.addTarget(UIButton.self, action: #selector(UIButton.grow(_:)), for: .touchDragOutside)
        self.addTarget(UIButton.self, action: #selector(UIButton.shrink(_:)), for: .touchDragEnter)
        self.addTarget(UIButton.self, action: #selector(UIButton.shrink(_:)), for: .touchDown)
    }
    
    public func waitForBounce(handler: (() -> ())? = nil)
    {
        self.isUserInteractionEnabled = false
        MadJohTools.delay(UIButton.boucneDuration)
        {
            UIButton.grow(self)
            
            MadJohTools.delay(UIButton.boucneDuration)
            {
                self.isUserInteractionEnabled = true
                
                handler?()
            }
        }
    }
    
    @objc static public func grow(_ view: UIButton) {
        UIView.animate(withDuration: boucneDuration, delay: 0, options: [.curveEaseInOut, .allowUserInteraction], animations:{
            view.transform = CGAffineTransform.identity
        }, completion: nil)
    }
    
    @objc static public func shrink(_ view: UIButton)
    {
        UIView.animate(withDuration: boucneDuration, delay: 0, options: [.curveEaseInOut, .allowUserInteraction], animations:{
            view.transform = CGAffineTransform(scaleX: bounceScale, y: bounceScale)
        }, completion: nil)
    }
}

public enum TransitionDirection
{
    case Left
    case Right
    case Up
    case Down
}
public extension UIViewController {
    public func performSegueToReturnBack(_ direction: TransitionDirection? = .Left)  {
        if let nav = self.navigationController {
            if let dir = direction {
                let t = CATransition()
                t.duration = 0.3
                t.type = kCATransitionMoveIn
                switch dir {
                case .Left:
                    t.subtype = kCATransitionFromLeft
                    break
                case .Right:
                    t.subtype = kCATransitionFromRight
                    break
                case .Up:
                    t.subtype = kCATransitionFromTop
                    break
                case .Down:
                    t.subtype = kCATransitionFromBottom
                    break
                }
                nav.view.layer.add(t, forKey: kCATransition)
            }
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    public static var top: UIViewController? {
        get {
            return topViewController()
        }
    }
    
    public static var root: UIViewController? {
        get {
            return UIApplication.shared.delegate?.window??.rootViewController
        }
    }
    
    public static func getTopViewController() -> UIViewController? {
        return topViewController()
    }
    
    private static func topViewController(from viewController: UIViewController? = UIViewController.root) -> UIViewController? {
        if let tabBarViewController = viewController as? UITabBarController {
            return topViewController(from: tabBarViewController.selectedViewController)
        } else if let navigationController = viewController as? UINavigationController {
            return topViewController(from: navigationController.visibleViewController)
        } else if let presentedViewController = viewController?.presentedViewController {
            return topViewController(from: presentedViewController)
        } else {
            return viewController
        }
    }
    
    public func createNewVc(withId id: String) -> UIViewController
    {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: id)
        return viewController
    }
    
    public func pushVc(_ vc: UIViewController, customTransition: CATransition? = nil, direction: TransitionDirection? = .Right, animated: Bool = true)
    {
        if let navigator = navigationController {
            if let transition = customTransition {
                navigator.view.layer.add(transition, forKey: kCATransition)
            }
            else if let dir = direction {
                let t = CATransition()
                t.duration = 0.3
                t.type = kCATransitionMoveIn
                switch dir {
                case .Left:
                    t.subtype = kCATransitionFromLeft
                    break
                case .Right:
                    t.subtype = kCATransitionFromRight
                    break
                case .Up:
                    t.subtype = kCATransitionFromTop
                    break
                case .Down:
                    t.subtype = kCATransitionFromBottom
                    break
                }
                navigator.view.layer.add(t, forKey: kCATransition)
            }
            navigator.pushViewController(vc, animated: customTransition == nil ? animated : false)
        }
    }
    
    public func present(_ viewControllerToPresent: UIViewController)
    {
        present(viewControllerToPresent, animated: true, completion: nil)
    }
}

public extension SKNode
{
    public var x:CGFloat {
        get {
            return self.position.x
        }
        set {
            self.position.x = newValue
        }
    }
    
    public var y:CGFloat {
        get {
            return self.position.y
        }
        set {
            self.position.y = newValue
        }
    }
    
    public var height:CGFloat {
        get {
            return self.frame.size.height
        }
    }
    
    public var width:CGFloat {
        get {
            return self.frame.size.width
        }
    }
}

public extension SKSpriteNode
{
    public convenience init(texture: SKTexture, width: CGFloat)
    {
        self.init(texture: texture)
        let ratio = self.height / self.width
        self.size = CGSize(width: width, height: width * ratio)
    }
    
    public convenience init(texture: SKTexture, height: CGFloat)
    {
        self.init(texture: texture)
        let ratio = self.width / self.height
        self.size = CGSize(width: height * ratio, height: height)
    }
    
    public func animateForEver(_ textures: [SKTexture])
    {
        self.run(SKAction.repeatForever(SKAction.animate(with: textures)))
    }
    
    public func animate(_ textures: [SKTexture], completion: (()->())? = nil)
    {
        guard let completion = completion else
        {
            self.run(SKAction.animate(with: textures))
            return
        }
        self.run(SKAction.animate(with: textures), completion: completion)
    }
    
    public func animate(_ textures: [SKTexture], duration: Double, completion: (()->())? = nil)
    {
        
        guard let completion = completion else
        {
            self.run(SKAction.animate(with: textures, duration: duration))
            return
        }
        self.run(SKAction.animate(with: textures, duration: duration), completion: completion)
    }
}

public extension SKAction
{
    public static func animate(with textures: [SKTexture], duration: TimeInterval) -> SKAction
    {
        let count = textures.count
        return SKAction.animate(with: textures, timePerFrame: duration / Double(count))
    }
    
    public static func animate(with textures: [SKTexture]) -> SKAction
    {
        return SKAction.animate(with: textures, timePerFrame: Double(1) / Double(24))
    }
}

public extension Array where Element: Equatable {
    // Remove first collection element that is equal to the given `object`:
    public mutating func remove(object: Element) {
        if let index = index(of: object) {
            self.remove(at: index)
        }
    }
}
