//
//  GameLoop.swift
//  MadJohTools
//
//  Created by Valentin Barat on 12/03/2017.
//  Copyright © 2017 MadJoh. All rights reserved.
//

import Foundation
import UIKit

public class GameLoop : NSObject
{
    public var doSomething: () -> ()!
    public var displayLink : CADisplayLink?
    public var frameInterval : Int!
    
    public init(frameInterval: Int, doSomething: @escaping () -> ())
    {
        self.doSomething = doSomething
        self.frameInterval = frameInterval
        super.init()
    }
    
    @objc private func handleTimer()
    {
        doSomething()
    }
    
    public func start()
    {
        displayLink = CADisplayLink(target: self, selector: #selector(GameLoop.handleTimer))
        displayLink!.frameInterval = frameInterval
        displayLink!.add(to: RunLoop.main, forMode: RunLoopMode.commonModes)
    }
    
    public func stop()
    {
        displayLink?.isPaused = true
        displayLink?.remove(from: RunLoop.main, forMode: RunLoopMode.defaultRunLoopMode)
        displayLink = nil
    }
}
